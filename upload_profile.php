<?php
/**
 * Created by PhpStorm.
 * User: Evgeny
 * Date: 29/04/2014
 * Time: 11:43 AM
 */

$isValid = false;
foreach (getallheaders() as $name => $value) {
    if ($name == "ClientKey" && $value=="678346293546726735642734691"){
        // echo "$name: $value\n";
        $isValid = true;
    }
}

if($isValid==false){
    exit;
}

if(!isset($_POST['data'])){
    exit;
}


include 'configdb.php';
include 'crypt.php';

$username = mysql_real_escape_string($_POST["username"]);
$deviceUUID = mysql_real_escape_string($_POST["deviceUUID"]);
$sessionKey = mysql_real_escape_string($_POST["sessionKey"]);

$sessionKeyHash= hash( 'sha256', $sessionKey );

$query = "SELECT id,user_id FROM evimedb.devices WHERE user_id=(SELECT id FROM evimedb.users WHERE username='$username') AND device_uuid='$deviceUUID' AND session_key='$sessionKeyHash'";

//echo "Query:__".$query."__";

$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

if($num==0){

    echo "{\"status\" : -3}";
    exit;

}


//echo "Auth Ok";

$json = base64_decode($_POST['data']);

$evimeData = json_decode($json, true);

$property = mysql_real_escape_string($evimeData["property"]);
$value = mysql_real_escape_string($evimeData["value"]);
$ownerUsername =  mysql_real_escape_string($evimeData["OwnerUsername"]);


$ownerUserId = 0;

$query = "SELECT id FROM evimedb.users WHERE username='$ownerUsername'";

//echo "Query:__".$query."__";

$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

if($num==0){

    echo "{\"status\" : -4}";
    exit;

}

if ($user = mysql_fetch_array($result, MYSQL_ASSOC)) {

    $ownerUserId = $user["id"];

}else{

    echo "{\"status\" : -4}";
    exit;

}

$query = "UPDATE evimedb.users SET full_name='$value' WHERE id='$ownerUserId'";

//echo "Query:__".$query."__";

$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}


echo "{\"status\" : 0}";
exit;

