<?php

// Put your device token here (without spaces):
/*
$isValid = false;
foreach (getallheaders() as $name => $value) {
    if ($name == "ClientKey" && $value=="678346293546726735642734691"){
        // echo "$name: $value\n";
        $isValid = true;
    }
}

if($isValid==false){
    exit;
}
*/

$deviceToken = 'cbab4828efcd81b792f745928f91ac060e4a37d02f4d0e75142bc255a0b0c6f6';

// Put your private key's passphrase here:
$passphrase = 'Prienote5';

// Put your alert message here:
$message = 'My first push notification!';

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'enotekitprodapnscert.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
	'ssl://gateway.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default'
	);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;


// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;


// Close the connection to the server
fclose($fp);



