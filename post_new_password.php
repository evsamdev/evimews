<?php
/**
 * Created by PhpStorm.
 * User: Evgeny
 * Date: 5/04/2014
 * Time: 4:59 PM
 */

include 'configdb.php';

$username = $_POST["username"];
$resetKey = $_POST["reset_key"];

$query = "SELECT username, id  FROM evimedb.users WHERE username='$username' AND reset_pass_key='$resetKey' AND reset_pass_key<>'NOPASSKEY'";
$result = mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

//echo "num = $num";

if ($num > 0) {

    $row = mysql_fetch_array($result);
    $user_name = $row["username"];
    $user_id = $row["id"];

}else{

    echo "Error! Please try again or contact support!";
    exit;
}

$new_password = $_POST["new_password"];

if(strlen($new_password)<6 || strlen($new_password)>20){

    echo "Error! Please try again!";
    exit;

}


$passKey=$username.$new_password."872345628765923765";

//echo $pass_key;
$authKey= hash( 'sha256', $passKey );

$authKeyHash =  hash( 'sha256', $authKey );

$query = "UPDATE evimedb.users SET auth_key='$authKeyHash',reset_pass_key='NOPASSKEY'  WHERE username='$username'";
$result = mysql_query($query);
if (!$result) {

    $message = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}


$query = "DELETE FROM evimedb.devices WHERE user_id=(SELECT id FROM evimedb.users WHERE username='$username')";
$result = mysql_query($query);
if (!$result) {

    $message = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Enotekit. New Password</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />

    <link rel="stylesheet" href="themes/dark_blue.css" />

    <link rel="stylesheet" href="styles/style.css" />

    <script type="text/javascript" src="scripts/jquery-1.6.4.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
    <script type="text/javascript" src="scripts/in-mobile.js"></script>

</head>
<body>

<div data-role="page" data-add-back-btn="true" data-theme="b" id="contact">

    <div data-role="header" data-theme="b">
        <h1>Enotekit</h1>
    </div><!-- Header -->

    <div data-role="content">
        <div class="ui-body ui-body-a">

            <div class="contact-form">

                <div data-role="fieldcontain" class="text-field">
                    <h2>Password has been changed!</h2>
                </div>

        </div>
    </div>
</div>
</body>
</html>