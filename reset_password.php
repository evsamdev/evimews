<?php
/**
 * Created by PhpStorm.
 * User: Evgeny
 * Date: 5/04/2014
 * Time: 3:01 PM
 */

$isValid = false;
foreach (getallheaders() as $name => $value) {
    if ($name == "ClientKey" && $value=="678346293546726735642734691"){
        // echo "$name: $value\n";
        $isValid = true;
    }
}


if($isValid==false){
    exit;
}


if(!isset($_POST['data'])){
    exit;
}

include 'configdb.php';

date_default_timezone_set('UTC');

$date_now = date("Y-m-d H:i:s");

$json = stripslashes($_POST['data']);

$accountData = json_decode($json, true);

$username = mysql_real_escape_string($accountData["username"]);
$passKey = mysql_real_escape_string($accountData["passKey"]);
$resetKey = mysql_real_escape_string($accountData["resetKey"]);

$myPass = $username."7236543";

$myPassHash = hash( 'sha256', $myPass);

if($passKey!=$myPassHash){

    echo "{\"status\" : -2}";
    exit;

}


$query = "SELECT id,email FROM evimedb.users WHERE username='$username'";
$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

if($num==0){

    echo "{\"status\" : -4}";
    exit;

}


$user = mysql_fetch_array($result, MYSQL_ASSOC);

$email=$user["email"];

$query = "UPDATE evimedb.users SET reset_pass_key='$resetKey' WHERE username='$username'";
$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}


$link = "https://enotekit.com/mobile_app/api/new_password.php?username=$username&reset_key=$resetKey";

$to      = $email;
$subject = 'Enotekit. Reset Password.';
$message = "To enter your new password please follow this link: $link";
$headers = "From: Enotekit Support <support@enotekit.com>";

mail($to, $subject, $message, $headers);

echo "{\"status\" : 0}";