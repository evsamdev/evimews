<?php
/**
 * Created by PhpStorm.
 * User: Evgeny
 * Date: 5/04/2014
 * Time: 4:51 PM
 */

include 'configdb.php';

$username = mysql_real_escape_string($_GET["username"]);
$resetKey = mysql_real_escape_string($_GET["reset_key"]);
//echo "user:".$username;
//echo "<br/>key".$resetKey;
$query = "SELECT username, id  FROM evimedb.users WHERE username='$username' AND reset_pass_key='$resetKey' AND reset_pass_key<>'NOPASSKEY'";
$result = mysql_query($query);
if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

//echo "num = $num";

if ($num > 0) {

    $row = mysql_fetch_array($result);
    $user_name = $row["username"];
    $user_id = $row["id"];

}else{

    echo "<h1>Wrong link</h1>";
    exit;
}

//echo "{\"status\" : 0}";
?>


<!DOCTYPE html>
<html>
<head>
    <title>Enotekit. New Password.</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />

    <link rel="stylesheet" href="themes/dark_blue.css" />

    <link rel="stylesheet" href="styles/style.css" />

    <script type="text/javascript" src="scripts/jquery-1.6.4.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
    <script type="text/javascript" src="scripts/in-mobile.js"></script>

</head>
<body>

<div data-role="page" data-add-back-btn="true" data-theme="b" id="contact">

    <div data-role="header" data-theme="b">
        <h1>Enotekit</h1>
    </div><!-- Header -->

    <div data-role="content">
        <div class="ui-body ui-body-a">

            <form action="post_new_password.php" method="POST">
            <div class="contact-form">
                <input type="hidden" name="reset_key" value="<?php echo $resetKey ?>"/>
                <input type="hidden" name="username" value="<?php echo $username ?>"/>
                <div data-role="fieldcontain" class="text-field">
                    <input type="password" name="new_password" id="main_pass" value="" placeholder="New Password (6-20)" class="required"/>
                </div>
                <div data-role="fieldcontain" class="text-field">
                    <input type="password" id="con_pass" name="surname" value="" placeholder="Repeat Password"/>
                </div>
                <div class="send"><a href="javascript:;" onclick="
    if(document.getElementById('main_pass').value!='')
    {
        if(document.getElementById('main_pass').value == document.getElementById('con_pass').value){

            if(document.getElementById('main_pass').value.length<6 || document.getElementById('main_pass').value.length>20){
                alert('New password must have length of 6-20 characters!');
            }else{
                document.forms[0].submit();
                return true;
            }
         }else{
            alert('Your passwords do not match.');
            return false;
         }
    }else{
        alert('Please enter new password.');
        return false;
    }" data-role="button" data-theme="a" data-iconpos="right" id="send-feedback">Submit!</a></div>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>