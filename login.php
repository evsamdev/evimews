<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Evgeny
 * Date: 23.09.13
 * Time: 0:04
 * To change this template use File | Settings | File Templates.
 */

$isValid = false;
foreach (getallheaders() as $name => $value) {
    if ($name == "ClientKey" && $value=="678346293546726735642734691"){
        // echo "$name: $value\n";
        $isValid = true;
    }
}

if($isValid==false){
    exit;
}


if(!isset($_POST['data'])){
    exit;
}

include 'configdb.php';

date_default_timezone_set('UTC');

$date_now = date("Y-m-d H:i:s");

$json = base64_decode($_POST['data']);

$accountData = json_decode($json, true);

$username = mysql_real_escape_string($accountData["username"]);
$passKey = mysql_real_escape_string($accountData["passKey"]);
$deviceUUID = mysql_real_escape_string($accountData["deviceUUID"]);
$deviceToken = mysql_real_escape_string($accountData["deviceToken"]);

$passKeyHash=hash( 'sha256', $passKey );

$query = "SELECT id,full_name,email FROM evimedb.users WHERE username='$username' AND auth_key='$passKeyHash'";
$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);
$user_id=0;
$full_name="";
$email="";
if($num==0){

    echo "{\"status\" : -2}";
    exit;

}

$row = mysql_fetch_array($result);
$user_id=$row["id"];
$full_name = $row["full_name"];

if ($user_id > 0) {

    $query = "DELETE FROM evimedb.devices WHERE user_id='$user_id' AND device_uuid='$deviceUUID'";
    $result=mysql_query($query);

    if (!$result) {

        $message  = 'Error: ' . mysql_error() . "\n";
        $message .= 'Query: ' . $query;
        die($message);

        //echo "{status : -1}";
    }

    if($deviceToken !=""){
        $query = "DELETE FROM evimedb.devices WHERE user_id='$user_id' AND device_token='$deviceToken'";
        $result=mysql_query($query);

        if (!$result) {

            $message  = 'Error: ' . mysql_error() . "\n";
            $message .= 'Query: ' . $query;
            die($message);

            //echo "{status : -1}";
        }
    }

    $newUID=$passKeyHash.uniqid();
    $sessionKey = hash('sha256', $newUID);
    $sessionKeyHash =  hash('sha256', $sessionKey);

    $query = "INSERT INTO evimedb.devices SET device_uuid='$deviceUUID', user_id = '$user_id', device_token='$deviceToken', session_key='$sessionKeyHash', auth_date='$date_now'";
    $result=mysql_query($query);

    if (!$result) {

        $message  = 'Error: ' . mysql_error() . "\n";
        $message .= 'Query: ' . $query;
        die($message);
        //echo "{status : -1}";
    }

    $response = array();
    $response["status"] = 0;
    $response["SessionKey"] = $sessionKey;
    $response["FullName"] = $full_name;

    //echo "{\"status\":0, \"SessionKey\": \"$sessionKey\",\"FullName\": \"$full_name\"}";

    print json_encode($response);



}else{

    echo "{\"status\" : -1}";

}







