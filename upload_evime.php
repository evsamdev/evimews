<?php
/**
 * Created by PhpStorm.
 * User: Evgeny
 * Date: 15/12/2013
 * Time: 6:45 PM
 */

$isValid = false;
foreach (getallheaders() as $name => $value) {
    if ($name == "ClientKey" && $value=="678346293546726735642734691"){
        // echo "$name: $value\n";
        $isValid = true;
    }
}

if($isValid==false){
    exit;
}

if(!isset($_POST['data'])){
    exit;
}


include 'configdb.php';
include 'crypt.php';

$username = mysql_real_escape_string($_POST["username"]);
$deviceUUID = mysql_real_escape_string($_POST["deviceUUID"]);
$sessionKey = mysql_real_escape_string($_POST["sessionKey"]);

$sessionKeyHash= hash( 'sha256', $sessionKey );

$query = "SELECT id,user_id FROM evimedb.devices WHERE user_id=(SELECT id FROM evimedb.users WHERE username='$username') AND device_uuid='$deviceUUID' AND session_key='$sessionKeyHash'";

//echo "Query:__".$query."__";

$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

if($num==0){

    echo "{\"status\" : -3}";
    exit;

}


//echo "Auth Ok";

$json =  base64_decode($_POST['data']);

$evimeData = json_decode($json, true);

if(isset($evimeData["FromUsername"])) $fromUsername = mysql_real_escape_string($evimeData["FromUsername"]);
if(isset($evimeData["ToUsername"]))$toUsername = mysql_real_escape_string($evimeData["ToUsername"]);
$ownerUsername =  mysql_real_escape_string($evimeData["OwnerUsername"]);

//echo "__".$fromFullNmame."__".$toUsername."__".$ownerUsername;

$uuid = mysql_real_escape_string($evimeData["UUID"]);
$createDate = mysql_real_escape_string($evimeData["CreateDate"]);
$deleteDate = mysql_real_escape_string($evimeData["DeleteDate"]);
$lasteditDate = mysql_real_escape_string($evimeData["LasteditDate"]);
$receiveDate = mysql_real_escape_string($evimeData["ReceiveDate"]);
$sendDate = mysql_real_escape_string($evimeData["SendDate"]);
$group = mysql_real_escape_string($evimeData["Group"]);
$trash = mysql_real_escape_string($evimeData["Trash"]);
$title = mysql_real_escape_string($evimeData["Title"]);
$box = mysql_real_escape_string($evimeData["Box"]);
$body = $evimeData["Body"];

$toUseId = 0;
$fromUseId = 0;
$ownerUserId = 0;

$query = "SELECT id FROM evimedb.users WHERE username='$ownerUsername'";

//echo "Query:__".$query."__";

$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

if($num==0){

    echo "{\"status\" : -4}";
    exit;

}

if ($user = mysql_fetch_array($result, MYSQL_ASSOC)) {

    $ownerUserId = $user["id"];

}


if (isset($toUsername)) {

    $query = "SELECT id FROM evimedb.users WHERE username='$toUsername'";

//echo "Query:__".$query."__";

    $result = mysql_query($query);

    if (!$result) {

        $message = 'Error: ' . mysql_error() . "\n";
        $message .= 'Query: ' . $query;
        die($message);

        //echo "{status : -1}";
    }

    $num = mysql_num_rows($result);

    if ($num > 0) {

        if ($user = mysql_fetch_array($result, MYSQL_ASSOC)) {

            $toUseId = $user["id"];

        }

    }

}

$fromFullNmame="";

if (isset($fromUsername)) {

    $query = "SELECT id,full_name FROM evimedb.users WHERE username='$fromUsername'";

//echo "Query:__".$query."__";

    $result = mysql_query($query);

    if (!$result) {

        $message = 'Error: ' . mysql_error() . "\n";
        $message .= 'Query: ' . $query;
        die($message);

        //echo "{status : -1}";
    }

    $num = mysql_num_rows($result);

    if ($num > 0) {

        if ($user = mysql_fetch_array($result, MYSQL_ASSOC)) {

            $fromUseId = $user["id"];
            $fromFullNmame = $user["full_name"];
        }

    }

}

$crypted = fnEncrypt($body, "124312341234124312341234");

$query = "SELECT uuid FROM evimedb.evimes WHERE uuid='$uuid'";

//echo "Query:__".$query."__";

$result=mysql_query($query);

if (!$result) {

    $message  = 'Error: ' . mysql_error() . "\n";
    $message .= 'Query: ' . $query;
    die($message);

    //echo "{status : -1}";
}

$num = mysql_num_rows($result);

if ($num > 0) {


    $query="UPDATE evimedb.evimes SET
            user_id = $ownerUserId,
            from_user_id=$fromUseId,
            to_user_id=$toUseId,
            uuid='$uuid',
            create_date = '$createDate',
            delete_date = '$deleteDate',
            lastedit_date = '$lasteditDate',
            receive_date = '$receiveDate',
            send_date = '$sendDate',
            box='$box',
            trash=$trash,
            `group`='$group',
            body='$crypted',
            title='$title' WHERE uuid='$uuid'";

    //echo($query);

    $result = mysql_query($query);

    if(!$result){

        $message  = 'Error: ' . mysql_error() . "\n";
        $message .= 'Query: ' . $query;
        die($message);

    }


}else{


//$query = "UPDATE evimes SET body='$body' WHERE uid='$uid'";



    //print_r(error_get_last());
    //echo "Encrypred: ".$crypted."</br>";

    $query="INSERT INTO evimedb.evimes SET
            user_id = $ownerUserId,
            from_user_id=$fromUseId,
            to_user_id=$toUseId,
            uuid='$uuid',
            create_date = '$createDate',
            delete_date = '$deleteDate',
            lastedit_date = '$lasteditDate',
            receive_date = '$receiveDate',
            send_date = '$sendDate',
            box='$box',
            trash=$trash,
            `group`='$group',
            body='$crypted',
            title='$title'";

    //echo($query);

    $result = mysql_query($query);

    if(!$result){

        $message  = 'Error: ' . mysql_error() . "\n";
        $message .= 'Query: ' . $query;
        die($message);

    }


    if($box=="inbox"){
    //insert query to the database
        //echo "Title: ".stripslashes($title)."____";
        if($fromFullNmame==""){
            $pushMessage= base64_encode($fromUsername."\r\n".stripslashes($title));
        }else{
            $pushMessage= base64_encode($fromUsername." (".$fromFullNmame.") "."\r\n".stripslashes($title));
        }
        //echo "Message: ".$pushMessage."____";
        //$pushMessage = urlencode($pushMessage);
        $url = "https://enotekit.com/mobile_app/api/push_notification.php";
        //echo "URL: ".$url."____";
        $data = array('userid' => $ownerUserId, 'message' => $pushMessage);

    // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "ClientKey: 678346293546726735642734691\r\n".
                             "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        //echo "Response: ".$response."____";


    }
}
echo "{\"status\" : 0}";
exit;

